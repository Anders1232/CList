/*
CList, a circular FIFO list in C.
	Copyright (C) 2018 Francisco Anderson Bezerra Rodrigues

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CList.h"
#include <stdlib.h>
#include <string.h>

CList* NewCList(size_t elementSize){
#ifdef CLIST_NEW_CLISTS_CAN_HAVE_NO_DATA
	CList* ret= malloc(sizeof(CList));
	if(NULL == ret){
		return ret;
	}
	ret.data=null;
	ret.elementSize= elementSize;
	ret.elementsCapacity= 0;
	ret.begin= 0;
	ret.lenght=0;
	return ret;
#else
	return NewCListWithCapacity(elementSize, 8);
#endif
}

static size_t GetRealPositionOnCList(CList *list, size_t position)
{
	return ( (list->begin+position)%list->elementsCapacity) * (list->elementSize);
}

CList* NewCListWithCapacity(size_t elementSize, size_t capacity){
	CList* ret= malloc(sizeof(CList));
	if(NULL == ret){
		return ret;
	}
	ret->data=malloc(elementSize * ( (capacity>=8) ? capacity : 8) );
	if(NULL == ret->data){
		free(ret);
		return NULL;
	}
	ret->elementSize= elementSize;
	ret->elementsCapacity= capacity;
	ret->begin= 0;
	ret->lenght=0;
	return ret;
}

#define ExpandListIfIsNecessaryForMoreElements(list)\
	if(list->elementsCapacity == list->lenght)\
	{\
		void* newPointer;\
		if(NULL != ( newPointer = realloc(list->data, ( (list->elementsCapacity)<<1) * list->elementSize ) ) )\
		{\
			if(0!=list->begin){\
				memcpy( &( list->data[(list->elementsCapacity)*(list->elementSize)] ), &(list->data[0]),(list->begin)*(list->elementSize));\
			}\
			list->elementsCapacity= list->elementsCapacity<<1;\
			list->data= newPointer;\
		}\
		else\
		{\
			return CLIST_ERROR_ALLOCATION;\
		}\
	}

CListResult CList_push_back(CList* list, void *element)
{
	ExpandListIfIsNecessaryForMoreElements(list);
	memcpy( &(list->data[ GetRealPositionOnCList(list, list->lenght) ] ), element, list->elementSize );
	list->lenght= list->lenght+1;
	return CLIST_SUCCESS;
}
/*
void CList_push_position(CList* list, void *element, size_t position)
{
	ExpandListIfIsNecessaryForMoreElements(list);
	char* data= list->data;
	size_t elementSize= list->elementSize;
	size_t position= list->position;
	size_t positionInData= position+list->begin;
	memmove(list->data[( (list->position)+1)* (list->elementSize) ], (list->position)*(list->elementSize),  );
}
*/
CListResult CList_pop_front(CList* list, void *outElement)
{
	memcpy(outElement, &( list->data[list->begin*list->elementSize] ), list->elementSize);
	list->begin= (list->begin+1)%list->elementsCapacity;
	list->lenght= list->lenght-1;
	return CLIST_SUCCESS;
}

void CList_pop_position(CList* list, void *outElement, size_t position);

CListResult CList_reserve(CList* list, size_t suggestedSize)
{
	if(list->elementsCapacity < suggestedSize){
		void* newPointer;
		size_t newCapacity= ( ( (list->elementsCapacity)<<1) < suggestedSize)? suggestedSize : (list->elementsCapacity) <<1 ;
		if(NULL != ( newPointer = realloc(list->data, ( newCapacity * list->elementSize ) ) ) )
		{
			if(0!=list->begin){
				memcpy( &( list->data[(list->elementsCapacity)*(list->elementSize)] ), list->data,(list->begin)*(list->elementSize));
			}
			list->elementsCapacity= newCapacity;
			list->data= newPointer;
		}
		else
		{
			return CLIST_ERROR_ALLOCATION;
		}
	}
	return CLIST_SUCCESS;
}

//void CList_resize(CList* list, size_t newSize);

bool CList_empty(CList* list)
{
	return 0 == list->lenght;
}

void* CList_GetPosition(CList* list, size_t position)
{
	return &(list->data[ GetRealPositionOnCList(list, position) ] );
}

void* CList_AtPosition(CList* list, size_t position)
{
	if(position < list->lenght)
	{
		return CList_GetPosition(list, position);
	}
	else
	{
		return NULL;
	}
}

void CList_clear(CList* list)
{
	list->begin=0;
	list->lenght=0;
}

void Delete_CList(CList* list)
{
	free(list->data);
	free(list);
}

#undef ExpandListIfIsNecessaryForMoreElements
