/*
CList, a circular FIFO list in C.
	Copyright (C) 2018 Francisco Anderson Bezerra Rodrigues

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stddef.h>
#include <stdbool.h>

enum cListResult
{
	CLIST_SUCCESS=0,
	CLIST_ERROR_ALLOCATION
};
typedef enum cListResult CListResult;

struct cList
{
	char* data;
	size_t elementSize;
	size_t elementsCapacity;
	size_t begin;
	size_t lenght;//in terms of elements
};

typedef struct cList CList;

CList* NewCList(size_t elementSize);
CList* NewCListWithCapacity(size_t elementSize, size_t capacity);
CListResult CList_push_back(CList* list, void *element);
void CList_push_position(CList* list, void *element, size_t position);//not efficient TODO
CListResult CList_pop_front(CList* list, void *outElement);
void CList_pop_position(CList* list, void *outElement, size_t position);//not efficient TODO
CListResult CList_reserve(CList* list, size_t suggestedSize);
void CList_resize(CList* list, size_t newSize);
bool CList_empty(CList* list);
void* CList_GetPosition(CList* list, size_t position);
void* CList_AtPosition(CList* list, size_t position);
void CList_clear(CList* list);
void Delete_CList(CList* list);


